echo Opening ECS System
echo ECS Monitoring Console
xterm -T "MUSEUM ENVIRONMENTAL CONTROL SYSTEM CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar ECSConsole " &

xterm -T "TEMPERATURE CONTROLLER CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/TemperatureController" &
xterm -T "TEMPERATURE SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/TemperatureSensor " &

xterm -T "HUMIDITY CONTROLLER CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/HumidityController" &
xterm -T "HUMIDITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/HumiditySensor" &


xterm -T "SECURITY CONTROLLER CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/SecurityController" &
xterm -T "SECURITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/SecuritySensor" &
xterm -T "MUSEUM SECURITY CONTROL SYSTEM  CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar SCSConsole " &

xterm -T "SECURITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/SprinklerController" &
xterm -T "SECURITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/FireAlarmController" &
xterm -T "SECURITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/FireSensor" &

xterm -T "MUSEUM SECURITY CONTROL SYSTEM  CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar MaintenanceConsole" &

echo Working...
set +v
