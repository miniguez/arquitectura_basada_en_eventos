/**
 * **************************************************************************************
 * File:SCSMonitor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * <p>
 * Description:
 * <p>
 * This class monitors the environmental control systems that control museum temperature and humidity. In addition to
 * monitoring the temperature and humidity, the SCSMonitor also allows a user to set the humidity and temperature
 * ranges to be maintained. If temperatures exceed those limits over/under alarm indicators are triggered.
 * <p>
 */

import instrumentation.*;
import common.Component;
import java.util.*;


public class SCSMonitor extends Thread
{


	MessageWindow mw = null;                    // This is the message window
	private int arm = 1;
	private boolean critical = true;
	Indicator di;                                // Door indicator
	Indicator wi;                               // Window indicator
	Indicator mi;                                // Motion indicator
	private String message;                    // Used to read queue
	Random random = new Random();
	int ID = random.nextInt(20)%(20+1);

	int windowBroken = 0;
	int doorOpen = 0;
	int motionDetected = 0;

	// for system B
	boolean fireAlarm = false;
	boolean sprinkler = false;
	long alarmTime = 0;
	long tempTime = 0;
	boolean confirm = false;
	boolean cancel = false;
	// end

	public SCSMonitor()
	{
	} //Constructor

	@Override
	public void run()
	{

		int delay = 1000;                // The loop delay (1 second)

		boolean isDone = false;            // Loop termination flag
		boolean on = true;                // Used to turn on heaters, chillers, humidifiers, and dehumidifiers
		boolean off = false;            // Used to turn off heaters, chillers, humidifiers, and dehumidifiers
		String message = "";            // Used to read queue

		mw = new MessageWindow("SCS Monitoring Console", 0, 0);
		di = new Indicator("SECURITY UNK", mw.getX() + mw.width(), 0);
		setArm(1);
		mw.writeMessage("Registered with the message manager.");


		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		Component component = new Component();

		while (!isDone)
		{
			component.postEvent(Component.BEAT, "SCSConsole-" + String.valueOf(ID) + "#SCSConsole allows a guard to arm and disarm the system");

			message = component.getEvent(Component.SECURITY);
			// Temperature reading
			if (message != null && arm == 1)
			{
				try
				{
					String temp = message;
					switch (temp)
					{
						case "1":
							//                                    doorOpen = 1;
							mw.writeMessage("Door Trigger from sensor: ");
							break;
						case "2":
							//                                    windowBroken = 1;
							mw.writeMessage("window Trigger from sensor: ");
							break;
						case "3":
							//                                    motionDetected = 1;
							mw.writeMessage("Motion Trigger from sensor: ");
							break;
						default:
							//                                    doorOpen = 0;
							//                                    windowBroken = 0;
							//                                    motionDetected = 0;
							mw.writeMessage("No Trigger from sensor: ");
					}

				} // try
				catch (Exception e)
				{
					mw.writeMessage("Error reading temperature: " + e);

				} // catch
			}



			message = component.getEvent(Component.FIRE_ALARM);
			if (message != null)
			{
				if (message.equals(Component.TURN_ON))
				{
					mw.writeMessage("Fire Trigger from sensor ");
				}

				if (message.equals(Component.TURN_OFF))
				{
					//fireAlarm = false;
					//Fire(fireAlarm);
					//mw.WriteMessage("No Fire from sensor ");
				}
			}


			// If the message == END then this is a signal that the simulation is to end.
			message = component.getEvent(Component.END);
			if (message != null)
			{
				if (message.equals(Component.END_SIGNAL))
				{
					try
					{
						isDone = true;
						mw.writeMessage("\n\nSimulation Stopped. \n");
						di.dispose();
						// Get rid of the indicators. The message panel is left for the
						// user to exit so they can see the last message posted.
						if (wi != null)
						{
							wi.dispose();
						}
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}

			if(fireAlarm && !sprinkler && !confirm && System.currentTimeMillis() - alarmTime >= 15000){
				sprinkler = true;
				confirm = true;
				cancel = false;
				mw.writeMessage("Fire Alarm! No input in 15s. Automatically turn on sprinkler. Now you can turn off it.");
				SprinklerControl(sprinkler);
			}


			// This delay slows down the sample rate to Delay milliseconds
			try
			{
				Thread.sleep(delay);
			} // try
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			} // catch


		} // while
	} // main


	public void setCritical(boolean critical)
	{
		this.critical = critical;
	}

	/**
	 * *************************************************************************
	 * Arguments: arm 1 or 0
	 *
	 * Returns: nothing
	 *
	 * Exceptions: None
	 *
	 * *************************************************************************
	 */
	public void setArm(int arm)
	{
		this.arm = arm;
		mw.writeMessage("setting Arm to:: " + arm);
		if (arm == 1) // temperature is below threshhold
		{
			mw.writeMessage("System Armed:: ");
			if (doorOpen == 1)
			{
				di.setLampColorAndMessage("Alarm Ringing", 3);
				Door(doorOpen == 1);
			}
			else if (windowBroken == 1)
			{
				di.setLampColorAndMessage("Alarm Ringing", 3);
				Window(windowBroken == 1);
			}
			else if (motionDetected == 1)
			{
				di.setLampColorAndMessage("Alarm Ringing", 3);
				Motion(motionDetected == 1);
			}
			else
			{
				di.setLampColorAndMessage("Alarm Not Ringing", 1);
			}
		}
		else
		{

			mw.writeMessage("System Disarmed:: ");
			di.setLampColorAndMessage("System Deactivated", 2);

		}
	}

	private void Door(boolean ON)
	{
		// Here we create the message.

		if (ON)
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.DOOR_ON);

		}
		else
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.DOOR_OFF);

		} // if


	} // Heater

	/**
	 * *************************************************************************
	 * CONCRETE METHOD:: Chiller Purpose: This method posts messages that will
	 * signal the temperature controller to turn on/off the chiller
	 *
	 * Arguments: boolean ON(true)/OFF(false) - indicates whether to turn the
	 * chiller on or off.
	 *
	 * Returns: none
	 *
	 * Exceptions: Posting to message manager exception
	 *
	 * *************************************************************************
	 */
	private void Window(boolean ON)
	{
		// Here we create the message.


		if (ON)
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.WINDOW_ON);

		}
		else
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.WINDOW_OFF);

		} // if


	} // Chiller

	/**
	 * *************************************************************************
	 * CONCRETE METHOD:: Chiller Purpose: This method posts messages that will
	 * signal the temperature controller to turn on/off the chiller
	 *
	 * Arguments: boolean ON(true)/OFF(false) - indicates whether to turn the
	 * chiller on or off.
	 *
	 * Returns: none
	 *
	 * Exceptions: Posting to message manager exception
	 *
	 * *************************************************************************
	 */
	private void Motion(boolean ON)
	{
		// Here we create the message.


		if (ON)
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.MOTION_ON);

		}
		else
		{

			Component component = new Component();
			component.postEvent(Component.SECURITY_CONTROLLER, Component.MOTION_OFF);

		} // if


	}

	// for system B
	private void Fire(boolean status)
	{
		// Here we create the message.


		if (status)
		{

			Component component = new Component();
			component.postEvent(Component.FIRE_CONTROLLER, Component.TURN_ON);

		}
		else
		{

			Component component = new Component();
			component.postEvent(Component.FIRE_CONTROLLER, Component.TURN_OFF);

		} // if


	}

	private void SprinklerControl(boolean status)
	{
		// Here we create the message.


		if (status)
		{


			Component component = new Component();
			component.postEvent(Component.SPRINKLER_CONTROLLER, Component.TURN_ON);

		}
		else
		{

			Component component = new Component();
			component.postEvent(Component.SPRINKLER_CONTROLLER, Component.TURN_OFF);

		} // if


	}


	/**
	 * This method posts an event that stops the environmental control system.
	 * Exceptions: Posting to event manager exception
	 */
	public void halt()
	{
		mw.writeMessage("***HALT MESSAGE RECEIVED - SHUTTING DOWN SYSTEM***");
		// Here we create the stop event.
		Component component = new Component();
		// Here we send the event to the event manager.
		try
		{
			component.postEvent(Component.END, "XXX");
		}
		catch (Exception e)
		{
			System.out.println("Error sending halt message:: " + e);
		}
	} // halt

	public void setWindowBroken(int windowBroken)
	{
		this.windowBroken = windowBroken;
		mw.writeMessage("Door window set to:: " + windowBroken);
		if (arm == 1)
		{
			updateAlarm();
			Window(windowBroken == 1);
			mw.writeMessage("Window data to Controller:: " + (windowBroken == 1));
		}
	}

	public void setDoorOpen(int doorOpen)
	{
		this.doorOpen = doorOpen;
		mw.writeMessage("Door data set to:: " + doorOpen);
		if (arm == 1)
		{
			updateAlarm();
			Door(doorOpen == 1);
			mw.writeMessage("Door data to Controller:: " + (doorOpen == 1));
		}
	}

	public void setMotionDetected(int motionDetected)
	{
		this.motionDetected = motionDetected;
		mw.writeMessage("Motion data set to:: " + motionDetected);
		if (arm == 1)
		{
			updateAlarm();
			Motion(motionDetected == 1);
			mw.writeMessage("Motion data to Controller:: " + (motionDetected == 1));
		}
	}

	public void updateAlarm()
	{
		mw.writeMessage("Coming to reset alarm:: ");
		if (doorOpen == 1 || windowBroken == 1 || motionDetected == 1)
		{
			di.setLampColorAndMessage("Alarm Ringing", 3);
		}
		else
		{
			di.setLampColorAndMessage("Alarm Not Ringing", 1);
		}
	}

	public void triggerFireAlarm()
	{
		this.fireAlarm = true;
		if (alarmTime == 0)
		{
			this.alarmTime = System.currentTimeMillis();
			mw.writeMessage("Fire Alarm! Please confirm or cancel sprinkler action. ");
			Fire(fireAlarm);
		}
		else
		{
			mw.writeMessage("Fire already alarmed.");
		}
	}

	public boolean confirmSprinkler()
	{
		if (this.fireAlarm == true)
		{
			this.sprinkler = true;
			this.confirm = true;
			this.cancel = false;
			mw.writeMessage("Fire Alarm! Confirm turning on sprinkler. Now you can turn off it.");
			SprinklerControl(sprinkler);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean holy()
	{
		if (this.fireAlarm == true)
		{
			this.fireAlarm = false;
			this.cancel = false;
			this.confirm = false;
			this.alarmTime = 0;
			this.sprinkler = false;
			mw.writeMessage("Cancel sprinkler. Fire alarm is off.");
			Fire(fireAlarm);
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean cancelSprinkler()
	{
		if (this.sprinkler == true)
		{
			this.sprinkler = false;
			this.alarmTime = 0;
			this.fireAlarm = false;
			this.confirm = false;
			this.cancel = true;
			mw.writeMessage("Turn off sprinkler.");
			SprinklerControl(sprinkler);
			//Fire(fireAlarm);
			return true;
		}
		else
		{
			return false;
		}
	}


} // SCSMonitor
