/**
 * **************************************************************************************
 * File:TemperatureSensor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class simulates a temperature sensor. It polls the event manager for
 * events corresponding to changes in state of the heater or chiller and reacts
 * to them by trending the ambient temperature up or down. The current ambient
 * room temperature is posted to the event manager.
 * **************************************************************************************
 */
package sensors;

import common.Component;
import instrumentation.MessageWindow;
import java.util.*;

public class TemperatureSensor extends Sensor implements Runnable
{

	private boolean heaterState = false;	// Heater state: false == off, true == on
	private boolean chillerState = false;	// Chiller state: false == off, true == on
	private float currentTemperature;		// Current simulated ambient room temperature
	private String message; 				// Used to read queue
	Random random = new Random();
	int ID = random.nextInt(20)%(20+1);

	private static TemperatureSensor INSTANCE = new TemperatureSensor();

	private TemperatureSensor()
	{
		super();
	}

	public void run()
	{

		// We create a message window. Note that we place this panel about 1/2 across
		// and 1/3 down the screen
		float winPosX = 0.5f;    //This is the X position of the message window in terms
		//of a percentage of the screen height
		float winPosY = 0.3f;    //This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Temperature Sensor", winPosX, winPosY);
		messageWin.writeMessage("Registered with the rabbitmq event manager.");


		messageWin.writeMessage("\nInitializing Temperature Simulation::");
		currentTemperature = (float) 50.00;
		if (coinToss())
		{
			driftValue = getRandomNumber() * (float) -1.0;
		}
		else
		{
			driftValue = getRandomNumber();
		} // if

		messageWin.writeMessage("   Initial Temperature Set:: " + currentTemperature);
		messageWin.writeMessage("   Drift Value Set:: " + driftValue);

		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		messageWin.writeMessage("Beginning Simulation... ");

		while (!isDone)
		{
			postEvent(Component.BEAT, "Temperature Sensor-" + String.valueOf(ID) + "#This is a process that simulates the action of a temperature sensor. ");

			// Post the current temperature
			postEvent(TEMPERATURE, currentTemperature + "");
			messageWin.writeMessage("Current Temperature::  " + currentTemperature + " F");
			// Get the message queue
			message = getEvent(TEMPERATURE_SENSOR);

			if (message != null)
			{
				switch (message)
				{
					case HEATER_ON:
						heaterState = true;
						break;
					case HEATER_OFF:
						heaterState = false;
						break;
					case CHILLER_ON:
						chillerState = true;
						break;
					case CHILLER_OFF:
						chillerState = false;
						break;
				}
			}

			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}



			// Now we trend the temperature according to the status of the
			// heater/chiller controller.
			if (heaterState)
			{
				currentTemperature += getRandomNumber();
			} // if heater is on
			if (!heaterState && !chillerState)
			{
				currentTemperature += driftValue;
			} // if both the heater and chiller are off
			if (chillerState)
			{
				currentTemperature -= getRandomNumber();
			} // if chiller is on
			// Here we wait for a 2.5 seconds before we start the next sample
			try
			{
				Thread.sleep(delay);
			} // try
			catch (Exception e)
			{
				messageWin.writeMessage("Sleep error:: " + e);
			}  // catch
		} // while
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (TemperatureSensor.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new TemperatureSensor();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static TemperatureSensor getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this sensor
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		TemperatureSensor sensor = TemperatureSensor.getInstance();
		sensor.run();
	}

}
