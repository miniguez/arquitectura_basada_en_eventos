/**
 * **************************************************************************************
 * File:SecuritySensor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class simulates a security sensor. It polls the message manager for messages corresponding to changes in state
 * of the door window and motion sensors and switches them off or on.
 * **************************************************************************************
 */
package sensors;

import common.Component;
import instrumentation.MessageWindow;

public class SecuritySensor extends Sensor implements Runnable
{

	boolean DoorTriggered = false;	// Door state: false == off, true == on
	boolean WindowTriggered = false;	// Window state: false == off, true == on
	boolean MSensorTriggered = false;	// Motion Sensor state: false == off, true == on

	private String message; 				// Used to read queue
	int sensor = 0;

	private static SecuritySensor INSTANCE = new SecuritySensor();

	private SecuritySensor()
	{
		super();
	}

	public void run()
	{

		// We create a message window. Note that we place this panel about 1/2 across
		// and 1/3 down the screen
		float WinPosX = 0.7f; 	//This is the X position of the message window in terms
		//of a percentage of the screen height
		float WinPosY = 0.0f; 	//This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Security Sensor", WinPosX, WinPosY);
		messageWin.writeMessage("Registered with the rabbitmq event manager.");


		messageWin.writeMessage("\nInitializing Security Sensor Simulation::");


		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		messageWin.writeMessage("Beginning Simulation... ");

		while (!isDone)
		{


			// Get the message queue
			message = getEvent(SECURITY_SENSOR);

			if (message != null)
			{
				switch (message)
				{
					case DOOR_ON:
						DoorTriggered = true;
						break;
					case DOOR_OFF:
						DoorTriggered = false;
						break;
					case WINDOW_ON:
						WindowTriggered = true;
						break;
					case WINDOW_OFF:
						WindowTriggered = false;
						break;
					case MOTION_ON:
						MSensorTriggered = true;
						break;
					case MOTION_OFF:
						MSensorTriggered = false;
						break;
				}
			}

			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}




			if (DoorTriggered)
			{
				postEvent(SECURITY, "1");
				messageWin.writeMessage("Door Triggerred:: ");

			}

			if (WindowTriggered)
			{
				postEvent(SECURITY, "2");
				messageWin.writeMessage("Window Triggerred:: ");

			}

			if (MSensorTriggered)
			{
				postEvent(SECURITY, "3");
				messageWin.writeMessage("Motion Triggerred:: ");

			}

			if (!DoorTriggered && !MSensorTriggered && !WindowTriggered) {
				postEvent(SECURITY, "0");
				messageWin.writeMessage("Current Sensor set (0 for off)::  ");
			}



			// Here we wait for a 2.5 seconds before we start the next sample
			try
			{
				Thread.sleep(delay);
			} // try
			catch (Exception e)
			{
				messageWin.writeMessage("Sleep error:: " + e);
			}  // catch
		} // while
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (SecuritySensor.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new SecuritySensor();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static SecuritySensor getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this sensor
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		SecuritySensor sensor = SecuritySensor.getInstance();
		sensor.run();
	}

}
