/**
 * **************************************************************************************
 * File:ECSMonitor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * <p>
 * Description:
 * <p>
 * This class monitors the environmental control systems that control museum
 * temperature and humidity. In addition to monitoring the temperature and
 * humidity, the ECSMonitor also allows a user to set the humidity and
 * temperature ranges to be maintained. If temperatures exceed those limits
 * over/under alarm indicators are triggered.
 * <p>
 * Internal Methods:
 * <p>
 * static private void heater(boolean ON )
 * static private void chiller(boolean ON )
 * static private void humidifier(boolean ON )
 * static private void dehumidifier(boolean ON )
 */

import instrumentation.*;
import common.Component;
import java.util.*;


public class ECSMonitor extends Thread
{

	private float tempRangeHigh = 100;	// These parameters signify the temperature and humidity ranges in terms
	private float tempRangeLow = 0;		// of high value and low values. The ECSmonitor will attempt to maintain
	private float humiRangeHigh = 100;	// this temperature and humidity. Temperatures are in degrees Fahrenheit
	private float humiRangeLow = 0;		// and humidity is in relative humidity percentage.
	MessageWindow messageWin = null;    // This is the message window
	Indicator tempIndicator;            // Temperature indicator
	Indicator humIndicator;				// Humidity indicator
	private String message;					// Used to read queue
	Random random = new Random();
	int ID = random.nextInt(20)%(20+1);

	public ECSMonitor()
	{
	} //Constructor

	@Override
	public void run()
	{
		float currentTemperature = 0;	// Current temperature as reported by the temperature sensor
		float currentHumidity = 0;		// Current relative humidity as reported by the humidity sensor
		int delay = 1000;				// The loop delay (1 second)
		boolean isDone = false;			// Loop termination flag
		boolean on = true;				// Used to turn on heaters, chillers, humidifiers, and dehumidifiers
		boolean off = false;            // Used to turn off heaters, chillers, humidifiers, and dehumidifiers
		String message = "";            // Used to read queue

		messageWin = new MessageWindow("ECS Monitoring Console", 0, 0);
		tempIndicator = new Indicator("TEMP UNK", messageWin.getX() + messageWin.width(), 0);
		humIndicator = new Indicator("HUMI UNK", messageWin.getX() + messageWin.width(), (int) (messageWin.height() / 2), 2);

		messageWin.writeMessage("Registered with the rabbitmq event manager.");


		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		Component component = new Component();

		while (!isDone)
		{

			component.postEvent(Component.BEAT, "ECSConsole-" + String.valueOf(ID) + "#ECSConsole displays the status of the temperature and humidity.");
			message = component.getEvent(Component.TEMPERATURE);
			// Temperature reading
			if (message != null)
			{
				try
				{
					currentTemperature = Float.parseFloat(message);
				}
				catch (Exception e)
				{
					messageWin.writeMessage("Error reading temperature: " + e);
				}
			}

			message = component.getEvent(Component.HUMIDITY);
			// Humidity reading
			if (message != null)
			{
				try
				{
					currentHumidity = Float.parseFloat(message);
				}
				catch (Exception e)
				{
					messageWin.writeMessage("Error reading humidity: " + e);
				}
			}

			// If the message == END then this is a signal that the simulation is to end.
			message = component.getEvent(Component.END);
			if (message != null)
			{
				if (message.equals(Component.END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
						// Get rid of the indicators. The message panel is left for the
						// user to exit so they can see the last message posted.
						humIndicator.dispose();
						tempIndicator.dispose();
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}


			messageWin.writeMessage("Temperature: " + currentTemperature + "F  Humidity: " + currentHumidity);

			// Check temperature and effect control as necessary

			if (currentTemperature < tempRangeLow)
			{ // temperature is below threshhold
				tempIndicator.setLampColorAndMessage("TEMP LOW", 3);
				heater(on);
				chiller(off);
			}
			else
			{
				if (currentTemperature > tempRangeHigh)
				{ // temperature is above threshhold
					tempIndicator.setLampColorAndMessage("TEMP HIGH", 3);
					heater(off);
					chiller(on);
				}
				else
				{
					tempIndicator.setLampColorAndMessage("TEMP OK", 1); // temperature is within threshhold
					heater(off);
					chiller(off);
				} // if
			} // if

			// Check humidity and effect control as necessary

			if (currentHumidity < humiRangeLow)
			{
				humIndicator.setLampColorAndMessage("HUMI LOW", 3); // humidity is below threshhold
				humidifier(on);
				dehumidifier(off);
			}
			else
			{
				if (currentHumidity > humiRangeHigh)
				{ // humidity is above threshhold
					humIndicator.setLampColorAndMessage("HUMI HIGH", 3);
					humidifier(off);
					dehumidifier(on);
				}
				else
				{
					humIndicator.setLampColorAndMessage("HUMI OK", 1); // humidity is within threshhold
					humidifier(off);
					dehumidifier(off);
				} // if
			} // if

			// This delay slows down the sample rate to Delay milliseconds
			try
			{
				Thread.sleep(delay);
			} // try
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			} // catch




		} // while
	} // main


	/**
	 * This method sets the temperature range
	 *
	 * @param lowtemp  low temperature range
	 * @param hightemp high temperature range
	 */
	public void setTemperatureRange(float lowtemp, float hightemp)
	{
		tempRangeHigh = hightemp;
		tempRangeLow = lowtemp;
		messageWin.writeMessage("***Temperature range changed to::" + tempRangeLow + "F - " + tempRangeHigh + "F***");
	} // setTemperatureRange

	/**
	 * This method sets the humidity range
	 *
	 * @param lowhumi  low humidity range
	 * @param highhumi high humidity range
	 */
	public void setHumidityRange(float lowhumi, float highhumi)
	{
		humiRangeHigh = highhumi;
		humiRangeLow = lowhumi;
		messageWin.writeMessage("***Humidity range changed to::" + humiRangeLow + "% - " + humiRangeHigh + "%***");
	} // setTemperatureRange

	/**
	 * This method posts an event that stops the environmental control system.
	 * Exceptions: Posting to event manager exception
	 */
	public void halt()
	{
		messageWin.writeMessage("***HALT MESSAGE RECEIVED - SHUTTING DOWN SYSTEM***");
		// Here we create the stop event.
		Component component = new Component();
		// Here we send the event to the event manager.
		try
		{
			component.postEvent(Component.END, "XXX");
		}
		catch (Exception e)
		{
			System.out.println("Error sending halt message:: " + e);
		}
	} // halt

	/**
	 * This method posts events that will signal the temperature controller to
	 * turn on/off the heater
	 *
	 * @param ON indicates whether to turn the heater on or off. Exceptions:
	 *           Posting to event manager exception
	 */
	private void heater(boolean ON)
	{
		Component component = new Component();
		try
		{
			if (ON)
			{
				component.postEvent(Component.TEMPERATURE_CONTROLLER, Component.HEATER_ON);
			}
			else
			{
				component.postEvent(Component.TEMPERATURE_CONTROLLER, Component.HEATER_OFF);
			} // if
		}
		catch (Exception e)
		{
			System.out.println("Error sending heater control message:: " + e);
		}

	} // heater

	/**
	 * This method posts events that will signal the temperature controller to
	 * turn on/off the chiller
	 *
	 * @param ON indicates whether to turn the chiller on or off. Exceptions:
	 *           Posting to event manager exception
	 */
	private void chiller(boolean ON)
	{
		// Here we create the event.
		Component component = new Component();
		try
		{
			if (ON)
			{
				component.postEvent(Component.TEMPERATURE_CONTROLLER, Component.CHILLER_ON);
			}
			else
			{
				component.postEvent(Component.TEMPERATURE_CONTROLLER, Component.CHILLER_OFF);
			} // if
		}
		catch (Exception e)
		{
			System.out.println("Error sending heater control message:: " + e);
		}

	} // Chiller

	/**
	 * This method posts events that will signal the humidity controller to turn
	 * on/off the humidifier
	 *
	 * @param ON indicates whether to turn the humidifier on or off. Exceptions:
	 *           Posting to event manager exception
	 */
	private void humidifier(boolean ON)
	{
		// Here we create the event.
		Component component = new Component();
		try
		{
			if (ON)
			{
				component.postEvent(Component.HUMIDITY_CONTROLLER, Component.HUMIDIFIER_ON);
			}
			else
			{
				component.postEvent(Component.HUMIDITY_CONTROLLER, Component.HUMIDIFIER_OFF);
			} // if
		}
		catch (Exception e)
		{
			System.out.println("Error sending humidifier control message:: " + e);
		}

	} // Humidifier

	/**
	 * This method posts events that will signal the humidity controller to turn
	 * on/off the dehumidifier
	 *
	 * @param ON indicates whether to turn the dehumidifier on or off.
	 *           Exceptions: Posting to event manager exception
	 */
	private void dehumidifier(boolean ON)
	{
		// Here we create the event.
		Component component = new Component();
		try
		{
			if (ON)
			{
				component.postEvent(Component.HUMIDITY_CONTROLLER, Component.DEHUMIDIFIER_ON);
			}
			else
			{
				component.postEvent(Component.HUMIDITY_CONTROLLER, Component.DEHUMIDIFIER_OFF);
			} // if
		}
		catch (Exception e)
		{
			System.out.println("Error sending dehumidifier control message:: " + e);
		}

	} // Dehumidifier
} // ECSMonitor
