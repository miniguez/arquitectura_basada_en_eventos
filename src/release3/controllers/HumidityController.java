/**
 * **************************************************************************************
 * File:HumidityController.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * **************************************************************************************
 * This class simulates a device that controls a humidifier and dehumidifier.
 * It polls the event manager for event ids = 4 and reacts to them by turning on or
 * off the humidifier/dehumidifier. The following command are valid strings for
 * controlling the humidifier and dehumidifier:
 *
 * H1 = humidifier on
 * H0 = humidifier off
 * D1 = dehumidifier on
 * D0 = dehumidifier off
 * **************************************************************************************
 */

package controllers;

import common.Component;
import instrumentation.Indicator;
import instrumentation.MessageWindow;
import java.util.*;

public class HumidityController extends Controller implements Runnable
{

	private boolean humidifierState = false;    // Heater state: false == off, true == on
	private boolean dehumidifierState = false;  // Dehumidifier state: false == off, true == on
	private String message;
	Random random = new Random();
	int ID = random.nextInt(20)%(20+1);// Used to read queue

	private static HumidityController INSTANCE = new HumidityController();

	private HumidityController()
	{
	}

	@Override
	public void run()
	{

		System.out.println("Registered with the rabbitmq event manager.");

		/* Now we create the humidity control status and message panel
		** We put this panel about 2/3s the way down the terminal, aligned to the left
        ** of the terminal. The status indicators are placed directly under this panel
        */
		float winPosX = 0.0f;    //This is the X position of the message window in terms
		//of a percentage of the screen height
		float winPosY = 0.60f;    //This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Humidity Controller Status Console", winPosX, winPosY);

		// Now we put the indicators directly under the humitity status and control panel
		Indicator humIndicator = new Indicator("Humid OFF", messageWin.getX(), messageWin.getY() + messageWin.height());
		Indicator dehumIndicator = new Indicator("DeHumid OFF", messageWin.getX() + (humIndicator.width() * 2), messageWin.getY() + messageWin.height());

		messageWin.writeMessage("Registered with the rabbitmq event manager.");


		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		while (!isDone)
		{
			postEvent(Component.BEAT, "Humidity Controller" + String.valueOf(ID) + "#The humidity controller is a process that interfaces with, and controls two devices: a humidifier to add moisture to the air, and a dehumidifier to remove moisture from the air.");

			message = getEvent(HUMIDITY_CONTROLLER);

			if (message != null)
			{
				if (message.equals(HUMIDIFIER_ON))
				{ // humidifier on
					humidifierState = true;
					messageWin.writeMessage("Received humidifier on event");

					// Confirm that the message was recieved and acted on
					postEvent(HUMIDITY_SENSOR, HUMIDIFIER_ON);
				}
				if (message.equals(HUMIDIFIER_OFF))
				{ // humidifier off
					humidifierState = false;
					messageWin.writeMessage("Received humidifier off event");

					// Confirm that the message was recieved and acted on
					postEvent(HUMIDITY_SENSOR, HUMIDIFIER_OFF);
				}
				if (message.equals(DEHUMIDIFIER_ON))
				{ // dehumidifier on
					dehumidifierState = true;
					messageWin.writeMessage("Received dehumidifier on event");

					// Confirm that the message was recieved and acted on
					postEvent(HUMIDITY_SENSOR, DEHUMIDIFIER_ON);
				}

				if (message.equals(DEHUMIDIFIER_OFF))
				{ // dehumidifier off
					dehumidifierState = false;
					messageWin.writeMessage("Received dehumidifier off event");

					// Confirm that the message was recieved and acted on
					postEvent(HUMIDITY_SENSOR, DEHUMIDIFIER_OFF);
				}
			}



			// If the event ID == END then this is a signal that the simulation is to end.

			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;

						messageWin.writeMessage("\n\nSimulation Stopped. \n");
						// Get rid of the indicators. The message panel is left for the
						// user to exit so they can see the last message posted.
						humIndicator.dispose();
						dehumIndicator.dispose();
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}


			}




			// Update the lamp status
			if (humidifierState)
			{
				// Set to green, humidifier is on
				humIndicator.setLampColorAndMessage("HUMID ON", 1);
			}
			else
			{
				// Set to black, humidifier is off
				humIndicator.setLampColorAndMessage("HUMID OFF", 0);
			}

			if (dehumidifierState)
			{
				// Set to green, dehumidifier is on
				dehumIndicator.setLampColorAndMessage("DEHUMID ON", 1);
			}
			else
			{
				// Set to black, dehumidifier is off
				dehumIndicator.setLampColorAndMessage("DEHUMID OFF", 0);
			}
			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			}
		}

	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (HumidityController.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new HumidityController();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static HumidityController getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this controller
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		HumidityController sensor = HumidityController.getInstance();
		sensor.run();
	}
}
