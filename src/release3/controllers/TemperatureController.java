/**
 * **************************************************************************************
 * File:TemperatureController.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class simulates a device that controls a heater and chiller.
 * It polls the event manager for event ids = 5 and reacts to them by turning
 * on or off the heater or chiller. The following command are valid strings for
 * controlling the heater and chiller:
 *
 * H1 = heater on
 * H0 = heater off
 * C1 = chiller on
 * C0 = chiller off
 * **************************************************************************************
 */

package controllers;

import common.Component;
import instrumentation.Indicator;
import instrumentation.MessageWindow;
import java.util.*;

public class TemperatureController extends Controller implements Runnable
{

	private boolean heaterState = false;	// Heater state: false == off, true == on
	private boolean chillerState = false;   // Chiller state: false == off, true == on
	private String message;					// Used to read queue
	Random random = new Random();
	int ID = random.nextInt(20)%(20+1);

	private static TemperatureController INSTANCE = new TemperatureController();

	private TemperatureController()
	{
	}

	@Override
	public void run()
	{

		System.out.println("Registered with the rabbitmq event manager.");

        /* Now we create the temperature control status and message panel
		 ** We put this panel about 1/3 the way down the terminal, aligned to the left
         ** of the terminal. The status indicators are placed directly under this panel
         */
		float winPosX = 0.0f;    //This is the X position of the message window in terms
		//of a percentage of the screen height
		float winPosY = 0.3f;    //This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Temperature Controller Status Console", winPosX, winPosY);

		// Put the status indicators under the panel...
		Indicator chillIndicator = new Indicator("Chiller OFF", messageWin.getX(), messageWin.getY() + messageWin.height());
		Indicator heatIndicator = new Indicator("Heater OFF", messageWin.getX() + (chillIndicator.width() * 2), messageWin.getY() + messageWin.height());

		messageWin.writeMessage("Registered with the rabbitmq event manager.");

		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */

		while (!isDone)
		{
			postEvent(Component.BEAT, "Temperature Controller-" + String.valueOf(ID) + "#The temperature Controller is used to control temperature. ");

			message = getEvent(TEMPERATURE_CONTROLLER);
			if (message != null)
			{
				if (message.equals(HEATER_ON))
				{ // heater on
					heaterState = true;
					postEvent(TEMPERATURE_SENSOR, HEATER_ON);
					messageWin.writeMessage("Received heater on event");
				}

				if (message.equals(HEATER_OFF))
				{ // heater off
					heaterState = false;
					postEvent(TEMPERATURE_SENSOR, HEATER_OFF);
					messageWin.writeMessage("Received heater off event");
				}

				if (message.equals(CHILLER_ON))
				{ // chiller on
					chillerState = true;
					postEvent(TEMPERATURE_SENSOR, CHILLER_ON);
					messageWin.writeMessage("Received chiller on event");
				}

				if (message.equals(CHILLER_OFF))
				{ // chiller off
					chillerState = false;
					postEvent(TEMPERATURE_SENSOR, CHILLER_ON);
					messageWin.writeMessage("Received chiller off event");
				}
			}


			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
						// Get rid of the indicators. The message panel is left for the
						// user to exit so they can see the last message posted.
						chillIndicator.dispose();
						heatIndicator.dispose();
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}

			
			// Update the lamp status
			if (heaterState)
			{
				// Set to green, heater is on
				heatIndicator.setLampColorAndMessage("HEATER ON", 1);
			}
			else
			{
				// Set to black, heater is off
				heatIndicator.setLampColorAndMessage("HEATER OFF", 0);
			}
			if (chillerState)
			{
				// Set to green, chiller is on
				chillIndicator.setLampColorAndMessage("CHILLER ON", 1);
			}
			else
			{
				// Set to black, chiller is off
				chillIndicator.setLampColorAndMessage("CHILLER OFF", 0);
			}
			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			}

		}
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (TemperatureController.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new TemperatureController();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static TemperatureController getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this controller
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		TemperatureController sensor = TemperatureController.getInstance();
		sensor.run();
	}

} // TemperatureController
