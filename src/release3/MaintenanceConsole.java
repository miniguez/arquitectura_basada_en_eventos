/**
 * **************************************************************************************
 * File:MaintenanceConsole.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * **************************************************************************************
 *
 * **************************************************************************************
 */

import common.*;
import java.util.*;

public class MaintenanceConsole
{

	public static void main(String args[])
	{
		IOManager userInput = new IOManager();    // IOManager IO Object
		boolean isDone = false;            // Main loop flag
		String option;                          // Menu choice from user
		boolean isError;                        // Error flag
		String message = "";            // Used to read queue

		int Delay = 5000;                   // The loop delay (5 seconds)



		//overall set
		Set<String> overAllSet = new HashSet<String>();
		Component component = new Component();

		while (!isDone)
		{
			System.out.println("-----Check Start-----");


			// If there are messages in the queue, we read through them.
			// We are looking for MessageIDs = 6, this is a request to turn one of the
			// 3 sensors. Note that we get all the messages
			// at once... there is a 2.5 second delay between samples,.. so
			// the assumption is that there should only be a message at most.
			// If there are more, it is the last message that will effect the
			// output of the temperature as it would in reality.


			Set<String> set = new HashSet<String>();
			message = component.getEvent(component.BEAT);
			int count = 0;

			if (message != null)
			{
				String tempMessage = message;
				if (set.contains(tempMessage))
					continue;
				else {
					count++;
					String[] description = tempMessage.split("#");
					System.out.println( " " + String.valueOf(count) + ". " + description[0] + " is on.");
					System.out.println( "   " +description[1]);
					set.add(tempMessage);
				}
				overAllSet.add(tempMessage);
			}

			// If the message == END then this is a signal that the simulation is to end.
			message = component.getEvent(Component.END);
			if (message != null)
			{
				if (message.equals(Component.END_SIGNAL))
				{
					try
					{
						isDone = true;
						//mw.writeMessage("\n\nSimulation Stopped. \n");

					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}


			//check for the health
			Iterator<String> it = overAllSet.iterator();
			while (it.hasNext()) {
				String str = it.next();
				if (set.contains(str))
					continue;
				else {
					String[] description = str.split("#");
					System.out.println( " * " + description[0] + " is off.");
					System.out.println( "   " + description[1]);
				}
			}

			System.out.println("-----Check End-----");

			try
			{
				Thread.sleep( Delay );

			} // try

			catch( Exception e )
			{
				System.out.println( "Sleep error:: " + e );

			} // catch


		} // while
	} // main
} // MaintenanceConsole
