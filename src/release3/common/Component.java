/**
 * **************************************************************************************
 * File:Component.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * **************************************************************************************
 * This class is the parent of all devices on this system, sensors and controllers.
 * Contains the IP address of the server where is the event manager.
 * **************************************************************************************
 */
package common;

import com.rabbitmq.client.*;
import com.rabbitmq.client.AMQP.BasicProperties;
import rabbit.RabbitmqEventManager;

import java.io.IOException;


public class Component
{

	public static String SERVER_IP = "127.0.0.1";

	public final static int TEMPERATURE = 1;
	public final static int HUMIDITY = 2;
	public final static int SECURITY = 3;
	public final static int FIRE_ALARM = 22;

	public final static int TEMPERATURE_SENSOR = -5;
	public final static int HUMIDITY_SENSOR = -4;
	public final static int TEMPERATURE_CONTROLLER = 5;
	public final static int HUMIDITY_CONTROLLER = 4;
	public final static int SECURITY_CONTROLLER = 6;
	public final static int SECURITY_SENSOR = -6;
	public final static int FIRE_CONTROLLER = 12;
	public final static int FIRE_SENSOR = -12;
	public final static int SPRINKLER_CONTROLLER = 13;
	public final static int SPRINKLER_SENSOR = -13;
	public final static int SPRINKLER = 44;
	public final static int BEAT = -100;


	public final static int END = 99;
	public final static String END_SIGNAL = "XXX";

	public final static String HEATER_ON = "H1";
	public final static String HEATER_OFF = "H0";
	public final static String CHILLER_ON = "C1";
	public final static String CHILLER_OFF = "C0";

	public final static String HUMIDIFIER_ON = "H1";
	public final static String HUMIDIFIER_OFF = "H0";
	public final static String DEHUMIDIFIER_ON = "D1";
	public final static String DEHUMIDIFIER_OFF = "D0";
	public final static String DOOR_ON = "DD1";
	public final static String DOOR_OFF = "DD0";
	public final static String WINDOW_ON = "W1";
	public final static String WINDOW_OFF = "W0";
	public final static String MOTION_ON = "M1";
	public final static String MOTION_OFF = "M0";
	public final static String TURN_ON = "ON";
	public final static String TURN_OFF = "OFF";



	private String event_response;

	private static final String EXCHANGE_NAME = "topic_logs";

	public Component()
	{
	}

	// This is to accomplish with singleton pattern
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * This method posts the specified temperature value to the specified event
	 * manager.
	 *
	 * @param ei      This is the eventmanger interface where the event will be
	 *                posted.
	 * @param eventId This is the ID to identify the type of event
	 * @param value   Is the value to publish in the event queue
	 */
	public void postEvent(int eventId, String message)
	{
		Connection connection = null;
		Channel channel = null;

		try
		{
			connection = RabbitmqEventManager.getConnection();
			channel = connection.createChannel();
			String routingKey = eventId + "";
			channel.exchangeDeclare(EXCHANGE_NAME, "topic");

			//channel.queueDeclare(routingKey, false, false, false, null);

			channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
			//System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");




		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally {
			if (connection!= null) {
				try {
					connection.close();
				}
				catch (Exception ignore) {}
			}
		}


	}


	public String getEvent(int eventId)
	{

		Connection connection = null;
		Channel channel = null;

		try
		{
			connection = RabbitmqEventManager.getConnection();
			channel = connection.createChannel();

			String routingKey = eventId + "";

			channel.exchangeDeclare(EXCHANGE_NAME, "topic");
			String queueName = channel.queueDeclare().getQueue();

			channel.queueBind(queueName, EXCHANGE_NAME, routingKey);

			//System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope,
										   AMQP.BasicProperties properties, byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					event_response = message;
					//System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
				}
			};
			channel.basicConsume(queueName, true, consumer);



		}
		catch (Exception e)
		{
			e.printStackTrace();
		}




		return event_response;
	}
}
