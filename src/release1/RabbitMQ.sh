#!/bin/bash
# description: rabbitmq Start Stop
case $1 in  
start)
rabbitmq-server -detached
;;   
stop)
rabbitmqctl stop
;;
esac      
exit 0