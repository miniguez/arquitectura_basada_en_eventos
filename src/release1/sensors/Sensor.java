/**
 * **************************************************************************************
 * File:Sensor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class contains the necessary to build a sensor, in order to every
 * sensor extends from this.
 * **************************************************************************************
 */
package sensors;

import common.Component;
import event.Event;

import java.util.Random;

import com.rabbitmq.client.*;
import com.rabbitmq.client.AMQP.BasicProperties;
import rabbit.RabbitmqEventManager;


public class Sensor extends Component
{

	protected int delay = 2500;                // The loop delay (2.5 seconds)
	protected boolean isDone = false;            // Loop termination flag
	protected float driftValue;                // The amount of temperature gained or lost

	protected Sensor()
	{
	}

	/**
	 * This method provides the simulation with random floating point
	 * temperature values between 0.1 and 0.9.
	 *
	 * @return A random number
	 */
	protected float getRandomNumber()
	{
		Random r = new Random();
		Float val;
		val = Float.valueOf((float) -1.0);
		while (val < 0.1)
		{
			val = r.nextFloat();
		}
		return (val.floatValue());
	} // GetRandomNumber

	/**
	 * This method provides a random true or
	 * false value used for determining the positiveness or negativeness of the
	 * drift value.
	 *
	 * @return A random boolean value
	 */
	protected boolean coinToss()
	{
		Random r = new Random();
		return (r.nextBoolean());
	} // CoinToss

}
