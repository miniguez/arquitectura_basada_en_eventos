package rabbit;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Channel;

public class RabbitmqEventManager
{

	public RabbitmqEventManager()
	{
	}

	private static Connection connection = null;


	// Return a connection if one doesn't exist. Else create a new one
	public static Connection getConnection() throws IOException, TimeoutException {
		if (connection == null) {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			factory.setUsername("guest");
			factory.setPassword("guest");
			factory.setRequestedHeartbeat(0);
			try
			{
				return factory.newConnection();
			}
			catch (IOException e)
			{
				System.out.println("RabbitMQ connection not acquired"+ e.getMessage());
			}
		}

		return connection;
	}






}
