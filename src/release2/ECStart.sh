echo Opening ECS System
echo ECS Monitoring Console
xterm -T "MUSEUM ENVIRONMENTAL CONTROL SYSTEM CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar ECSConsole " &

echo Opening Temperature Controller Console
xterm -T "TEMPERATURE CONTROLLER CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/TemperatureController" &
xterm -T "TEMPERATURE SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/TemperatureSensor " &

echo Opening Humidity Sensor Console
xterm -T "HUMIDITY CONTROLLER CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/HumidityController" &
xterm -T "HUMIDITY SENSOR CONSOLE" -e "java -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/HumiditySensor" &

echo Working...
set +v