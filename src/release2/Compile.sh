#!/bin/bash
echo Compiling files...
javac -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar common/*.java
javac -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar controllers/*.java
javac instrumentation/*.java
javac -cp .:commons-io-1.2.jar:commons-cli-1.1.jar:rabbitmq-client.jar sensors/*.java
javac *.java

echo Done. 
echo Press enter to continue...
read