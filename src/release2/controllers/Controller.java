/**
 * **************************************************************************************
 * File:Controller.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * **************************************************************************************
 * This class contains the necessary to build a controller, in order to every
 * controller extends from this.
 * **************************************************************************************
 */
package controllers;

import common.Component;
import event.Event;
import rabbit.RabbitmqEventManager;

public class Controller extends Component
{
	protected int delay = 2500;                // The loop delay (2.5 seconds)
	protected boolean isDone = false;            // Loop termination flag

	protected Controller()
	{
		super();
	}

}
