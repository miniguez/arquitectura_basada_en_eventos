/**
 * **************************************************************************************
 * File:SecurityController.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class simulates a device that controls a security system comprising of 3 sensors, window , door and motion detection. It polls the message
 * manager for message ids = 6
 * and reacts to them by turning on or off one of the three components. The following command are valid strings for controlling the security sensors:
 *
 *	D1 = Door break on
 *	D0 = Door break off
 * 	W1 = Window break on
 *	W0 = Window break off
 * 	M1 = motion detector on
 *	M0 = motion detector off
 *
 * The state (on/off) is graphically displayed on the terminal in the indicator. Command messages are displayed in
 * the message window. Once a valid command is relieved a confirmation message is sent with the id of -6 and the command in
 * the command string.
 * **************************************************************************************
 */

package controllers;

import common.Component;
import instrumentation.Indicator;
import instrumentation.MessageWindow;

public class SecurityController extends Controller implements Runnable
{

	boolean WindowState = false;		// Window state: false == off, true == on
	boolean DoorState = false;		// Door state: false == off, true == on
	boolean MSensorState = false;		// Motion Sensor state: false == off, true == on
	private String message;					// Used to read queue

	private static SecurityController INSTANCE = new SecurityController();

	private SecurityController()
	{
	}

	@Override
	public void run()
	{

		System.out.println("Registered with the rabbitmq event manager.");

        /* Now we create the temperature control status and message panel
		 ** We put this panel about 1/3 the way down the terminal, aligned to the left
         ** of the terminal. The status indicators are placed directly under this panel
         */
		float WinPosX = 0.4f; 	//This is the X position of the message window in terms
		//of a percentage of the screen height
		float WinPosY = 0.0f; 	//This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Security Controller Status Console", WinPosX, WinPosY);

		// Put the status indicators under the panel...
		Indicator di = new Indicator("Door Not Triggered", messageWin.getX(), messageWin.getY() + messageWin.height());
		Indicator wi = new Indicator("Window Not Triggered", messageWin.getX()+(di.width()*2), messageWin.getY()+messageWin.height());
		Indicator mi = new Indicator ("MSensors Not Triggered", messageWin.getX()+(wi.width()*4), messageWin.getY()+messageWin.height());

		messageWin.writeMessage("Registered with the rabbitmq event manager.");

		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */

		while (!isDone)
		{
			message = getEvent(SECURITY_CONTROLLER);
			if (message != null)
			{
				if (message.equals(DOOR_ON))
				{ // Door on
					DoorState = true;
					postEvent(SECURITY_SENSOR, DOOR_ON);
					messageWin.writeMessage("Received Door on event");
				}

				if (message.equals(DOOR_OFF))
				{ // Door off
					DoorState = false;
					postEvent(SECURITY_SENSOR, DOOR_OFF);
					messageWin.writeMessage("Received Door off event");
				}

				if (message.equals(WINDOW_ON))
				{ // window on
					WindowState = true;
					postEvent(SECURITY_SENSOR, WINDOW_ON);
					messageWin.writeMessage("Received window on event");
				}

				if (message.equals(WINDOW_OFF))
				{ // window off
					WindowState = false;
					postEvent(SECURITY_SENSOR, WINDOW_OFF);
					messageWin.writeMessage("Received window off event");
				}

				if (message.equals(MOTION_ON))
				{ // Motion Sensor on
					MSensorState = true;
					postEvent(SECURITY_SENSOR, MOTION_ON);
					messageWin.writeMessage("Received Motion Sensor on event");
				}

				if (message.equals(MOTION_OFF))
				{ // Motion Sensor off
					MSensorState = false;
					postEvent(SECURITY_SENSOR, MOTION_OFF);
					messageWin.writeMessage("Received Motion Sensor off event");
				}
			}


			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
						// Get rid of the indicators. The message panel is left for the
						// user to exit so they can see the last message posted.
						di.dispose();
						wi.dispose();
						mi.dispose();
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}

			
			// Update the lamp status
			if (DoorState)
			{
				// Set to green, Door is on
				di.setLampColorAndMessage("Door Triggered", 3);
			}
			else
			{
				// Set to black, Door is off
				di.setLampColorAndMessage("Door Intact", 1);
			}

			if (WindowState)
			{
				// Set to green, chiller is on
				wi.setLampColorAndMessage("Window Triggered", 3);
			}
			else
			{
				// Set to black, chiller is off
				wi.setLampColorAndMessage("Window Intact", 1);
			}

			if (MSensorState)
			{
				// Set to green, chiller is on
				mi.setLampColorAndMessage("MSensor Triggered", 3);
			}
			else
			{
				// Set to black, chiller is off
				mi.setLampColorAndMessage("MSensor Intact", 1);
			}


			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			}

		}
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (SecurityController.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new SecurityController();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static SecurityController getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this controller
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		SecurityController sensor = SecurityController.getInstance();
		sensor.run();
	}

} // TemperatureController
