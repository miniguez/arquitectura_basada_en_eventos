/**
 * **************************************************************************************
 * File:FireAlarmController.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * Description:
 *
 * Control the fire alarm
 * ON
 * **************************************************************************************
 */

package controllers;

import common.Component;
import instrumentation.Indicator;
import instrumentation.MessageWindow;

public class FireAlarmController extends Controller implements Runnable
{

	private String message;					// Used to read queue
	boolean state = false;
	private static FireAlarmController INSTANCE = new FireAlarmController();

	private FireAlarmController()
	{
	}

	@Override
	public void run()
	{

		System.out.println("Registered with the rabbitmq event manager.");

        /* Now we create the temperature control status and message panel
		 ** We put this panel about 1/3 the way down the terminal, aligned to the left
         ** of the terminal. The status indicators are placed directly under this panel
         */
		float WinPosX = 0.4f; 	//This is the X position of the message window in terms
		//of a percentage of the screen height
		float WinPosY = 0.6f; 	//This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow mw = new MessageWindow("Fire Alarm Controller Status Console", WinPosX, WinPosY);

		// Put the status indicators under the panel...
		Indicator di = new Indicator ("No Fire Alarm", mw.getX(), mw.getY()+mw.height());

		mw.writeMessage("Registered with the rabbitmq event manager.");

		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */

		while (!isDone)
		{
			message = getEvent(FIRE_CONTROLLER);
			if (message != null)
			{
				if (message.equals(TURN_ON))
				{
					state = true;
					postEvent(FIRE_SENSOR, TURN_ON);
					mw.writeMessage("Received fire event");
				}

				if (message.equals(TURN_OFF))
				{
					state = false;
					postEvent(FIRE_SENSOR, TURN_OFF);
					mw.writeMessage("Received no fire event");
				}



			}

			message = getEvent(SPRINKLER_CONTROLLER);
			if (message == TURN_ON)
			{
				if (message.equals(TURN_ON))
				{
					state = false;
					postEvent(FIRE_SENSOR, TURN_OFF);
					mw.writeMessage("Received sprinkler on event");
				}


			}

			message = getEvent(SPRINKLER);
			if (message == TURN_ON)
			{
				if (message.equals(TURN_ON))
				{
					state = false;
					postEvent(FIRE_SENSOR, TURN_OFF);
					mw.writeMessage("Received sprinkler on event");
				}


			}


			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						mw.writeMessage("\n\nSimulation Stopped. \n");
						di.dispose();
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}

			
			// Update the lamp status
			if (state)
			{
				// Set to green, Door is on
				di.setLampColorAndMessage("Fire Alarm On", 3);
			}
			else
			{
				// Set to black, Door is off
				di.setLampColorAndMessage("Fire Alarm Off", 1);
			}




			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e)
			{
				System.out.println("Sleep error:: " + e);
			}

		}
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (FireAlarmController.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new FireAlarmController();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static FireAlarmController getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this controller
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		FireAlarmController sensor = FireAlarmController.getInstance();
		sensor.run();
	}

} // TemperatureController
