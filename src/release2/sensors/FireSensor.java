/**
 * **************************************************************************************
 * File:FireSensor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 *
 * **************************************************************************************
 * This class simulates a fire sensor. 
 * **************************************************************************************
 */
package sensors;

import common.Component;
import instrumentation.MessageWindow;

public class FireSensor extends Sensor implements Runnable
{

	boolean state = false;	// Door state: false == off, true == on

	private String message; 				// Used to read queue
	int sensor = 0;

	private static FireSensor INSTANCE = new FireSensor();

	private FireSensor()
	{
		super();
	}

	public void run()
	{

		// We create a message window. Note that we place this panel about 1/2 across
		// and 1/3 down the screen
		float WinPosX = 0.7f; 	//This is the X position of the message window in terms
		//of a percentage of the screen height
		float WinPosY = 0.3f; 	//This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow mw = new MessageWindow("Fire Sensor", WinPosX, WinPosY);
		mw.writeMessage("Registered with the rabbitmq event manager.");


		mw.writeMessage("\nInitializing Security Sensor Simulation::");


		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		mw.writeMessage("Beginning Simulation... ");

		while (!isDone)
		{


			// Get the message queue
			message = getEvent(FIRE_SENSOR);

			if (message != null)
			{
				switch (message)
				{
					case TURN_ON:
						state = true;
						break;
					case TURN_OFF:
						state = false;
						break;

				}
			}

			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						mw.writeMessage("\n\nSimulation Stopped. \n");
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}




			if (state)
			{
				postEvent(FIRE_ALARM, TURN_ON);
				mw.writeMessage("Fire alarm");

			}
			else
			{
				//PostFire(em, "OFF");
				mw.writeMessage("No Fire");
			}




			// Here we wait for a 2.5 seconds before we start the next sample
			try
			{
				Thread.sleep(delay);
			} // try
			catch (Exception e)
			{
				mw.writeMessage("Sleep error:: " + e);
			}  // catch
		} // while
	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (FireSensor.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new FireSensor();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static FireSensor getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this sensor
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{
		FireSensor sensor = FireSensor.getInstance();
		sensor.run();
	}

}
