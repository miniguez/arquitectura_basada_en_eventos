/**
 * **************************************************************************************
 * File:HumiditySensor.java
 * Course: Software Architecture
 * Project: Event Architectures
 * Institution: Center for Research in Mathematics
 * Date: April 2016
 * Developers: Castillo Castañeda Pablo Mar, Íniguez González Miguel Ángel, Sobrevilla Dominguez Gabriela Mayanin
 * Reviewer: Perla Velasco Elizondo
 * **************************************************************************************
 * This class simulates a humidity sensor. It polls the event manager for events
 * corresponding to changes in state of the humidifier or dehumidifier and
 * reacts to them by trending the relative humidity up or down. The current
 * relative humidity is posted to the event manager.
 * **************************************************************************************
 */
package sensors;

import common.Component;
import event.EventManagerInterface;
import instrumentation.MessageWindow;

public class HumiditySensor extends Sensor implements Runnable
{

	private boolean humidifierState = false;    // Humidifier state: false == off, true == on
	private boolean dehumidifierState = false;    // Dehumidifier state: false == off, true == on
	private float relativeHumidity;        // Current simulated ambient room humidity
	private String message;                // Used to read queue

	private static HumiditySensor INSTANCE = new HumiditySensor();

	private HumiditySensor()
	{
	}

	@Override
	public void run()
	{


		// We create a message window. Note that we place this panel about 1/2 across
		// and 2/3s down the screen
		float winPosX = 0.5f;    //This is the X position of the message window in terms
		//of a percentage of the screen height
		float winPosY = 0.60f;    //This is the Y position of the message window in terms
		//of a percentage of the screen height

		MessageWindow messageWin = new MessageWindow("Humidity Sensor", winPosX, winPosY);
		messageWin.writeMessage("Registered with the rabbitmq event manager.");


		messageWin.writeMessage("\nInitializing Humidity Simulation::");
		relativeHumidity = getRandomNumber() * (float) 100.00;
		if (coinToss())
		{
			driftValue = getRandomNumber() * (float) -1.0;
		}
		else
		{
			driftValue = getRandomNumber();
		}
		messageWin.writeMessage("   Initial Humidity Set:: " + relativeHumidity);
		messageWin.writeMessage("   Drift Value Set:: " + driftValue);

		/**
		 * ******************************************************************
		 ** Here we start the main simulation loop
		 * *******************************************************************
		 */
		messageWin.writeMessage("Beginning Simulation... ");
		while (!isDone)
		{
			// Post the current relative humidity
			postEvent(HUMIDITY, relativeHumidity+ "");
			messageWin.writeMessage("Current Relative Humidity:: " + relativeHumidity + "%");
			// Get the message queue
			message = getEvent(HUMIDITY_SENSOR);

			if (message != null)
			{
				switch (message)
				{
					case HUMIDIFIER_ON:
						humidifierState = true;
						break;
					case HUMIDIFIER_OFF:
						humidifierState = false;
						break;
					case DEHUMIDIFIER_ON:
						dehumidifierState = true;
						break;
					case DEHUMIDIFIER_OFF:
						dehumidifierState = false;
						break;
				}
			}


			// If the event ID == END then this is a signal that the simulation is to end.
			message = getEvent(END);
			if (message != null)
			{
				if (message.equals(END_SIGNAL))
				{
					try
					{
						isDone = true;
						messageWin.writeMessage("\n\nSimulation Stopped. \n");
					}
					catch (Exception e)
					{
						System.out.println(e);
					}
				}

			}


			// Now we trend the relative humidity according to the status of the
			// humidifier/dehumidifier controller.
			if (humidifierState)
			{
				relativeHumidity += getRandomNumber();
			} // if humidifier is on

			if (!humidifierState && !dehumidifierState)
			{
				relativeHumidity += driftValue;
			} // if both the humidifier and dehumidifier are off

			if (dehumidifierState)
			{
				relativeHumidity -= getRandomNumber();
			} // if dehumidifier is on

			// Here we wait for a 2.5 seconds before we start the next sample
			try
			{
				Thread.sleep(delay);
			}
			catch (Exception e)
			{
				messageWin.writeMessage("Sleep error:: " + e);
			}
		}

	}

	private static void createInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (HumiditySensor.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new HumiditySensor();
				}
			}
		}
	}

	/**
	 * This method calls createInstance method to creates and ensure that
	 * only one instance of this class is created. Singleton design pattern.
	 *
	 * @return The instance of this class.
	 */
	public static HumiditySensor getInstance()
	{
		if (INSTANCE == null)
		{
			createInstance();
		}
		return INSTANCE;
	}

	/**
	 * Start this sensor
	 *
	 * @param args IP address of the event manager (on command line).
	 *             If blank, it is assumed that the event manager is on the local machine.
	 */
	public static void main(String args[])
	{

		HumiditySensor sensor = HumiditySensor.getInstance();
		sensor.run();
	}

} 
